	var finished=0;
	var yScales;
	var dataset;
	var colorScale;
	var xScale;
		d3.csv("data/salaries.csv", function(data) {
			finished=1;
			var dataset = [ 5, 10, 13, 19, 21, 25, 22, 18, 15, 13,
	11, 12, 15, 20, 18, 17, 16, 18, 23, 25 ];
							//dataset = dataset.concat(dataset);

							dataset = data;
							//dataset = dataset.slice(0,100)
							var bins = 5;
							//dataset = dataset.map(function(d){return d.Base;});
							dataset = d3.layout.histogram()
								.value(function(d){return parseInt(d["Base"])+parseInt(d["OT"])})
								.bins(bins)(dataset);

							var w = 800;
							var h = 400;
							var padding = 30;
							var barPadding = 1;
							var maxInBins = d3.max(dataset.map( 
								function(d){
									return d.y;
								}));

							xScale = d3.scale.ordinal()
							.domain(dataset.map(function(d){return d.x}))
							.rangeRoundBands([0,w], 0.05);

							var yScale = d3.scale.linear()
							.domain([0, maxInBins])
							.range([0,h-padding]);

							datas = dataset;

							colorScale = d3.scale.linear()
							.domain([0, maxInBins])
							.range([0, 255]);

							yScales = yScale;
							var maxValue = 100;


							var test = function(num){
								num = Math.floor(num/1000);
								num = num * 1000
								return d3.format("s")(num);
							}


								//Define X axis
					var xAxis = d3.svg.axis()
											.scale(xScale)
											.tickFormat(test)
											.orient("bottom")

						//Define Y axis
					var yAxis = d3.svg.axis()
											.scale(yScale)
											.orient("left")
											.ticks(5);

							var svg = d3.select("body")
							.append("svg")
							.attr("width", w)
							.attr("height", h);

							svg.selectAll("rect")
							.data(dataset)
							.enter()
							.append("rect")
							.attr("class", "rect")
							.attr({
								x: function ( d, i) {
									return xScale(d.x);
								},
								y: function ( d,i ) {
									return h - padding - yScale(d.y);
								},
								width: xScale.rangeBand(),
								height: function ( d ) {
									return yScale(d.y);
								},
								fill: function ( d ) {
									return "rgb(0, 0, " + Math.floor(colorScale(d.y)) + ")";
								}
							})

							 svg.append("g")
							.attr("class", "x axis")
							.attr("transform", "translate(0,"+ (h-padding) + " )")
							.call(xAxis)

				dataset = _.filter(data, function (worker) {
					return parseInt(worker["Base"]) + parseInt(worker["OT"]) > 30000;
				}) 
				h = 1000;
				w = 1000;

				console.log("MAXXXX: " + d3.max(dataset, function(d) { return parseInt(d["Base"]); }));


			//Create scale functions
			var xScale = d3.scale.pow().exponent(0.5)
								 .domain([ 
										d3.min(dataset, function(d) { return parseInt(d["Base"]); }),
										d3.max(dataset, function(d) { return parseInt(d["Base"]); })
										])
								 .range([padding, w - padding * 2]);

			var yScale = d3.scale.pow().exponent(0.3)
								 .domain([ 
									d3.min(dataset, function(d) { return parseInt(d["OT"]); }),
									d3.max(dataset, function(d) { return parseInt(d["OT"]); })
								 ])
								 .range([h - padding, padding]);
			var rScale = d3.scale.linear()
								 .domain([ 
									d3.min(dataset, function(d) { return parseInt(d["OT"])+parseInt(d["Base"]); }),
									d3.max(dataset, function(d) { return parseInt(d["OT"])+parseInt(d["Base"]); })
								 ])
								 .range([1, 16]);

			//Define X axis
			var xAxis = d3.svg.axis()
								.scale(xScale)
								.orient("bottom")
								.tickFormat(test);

			//Define Y axis
			var yAxis = d3.svg.axis()
								.scale(yScale)
								.orient("left")
								.tickFormat(test);

			//Create SVG element
			var svg = d3.select("body")
						.append("svg")
						.attr("width", w)
						.attr("height", h);

			//Create circles
			svg.append("g")
				 .attr("id", "circles")
				 .selectAll("circle")
				 .data(dataset)
				 .enter()
				 .append("circle")
				 .transition()
				 .duration(2000)
				 .attr("cx", function(d) {
						return xScale(parseInt(d["Base"]));
				 })
				 .attr("cy", function(d) {
						return yScale(parseInt(d["OT"]));
				 })
				 .attr("r", function(d){
					return rScale(parseInt(d["OT"])+parseInt(d["Base"]));
				 })
				 .attr("fill", "red");
			
			//Create X axis
			svg.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + (h - padding) + ")")
				.call(xAxis);
			
			//Create Y axis
			svg.append("g")
				.attr("class", "y axis")
				.attr("transform", "translate(" + padding + ",0)")
				.call(yAxis);

		})
		function meat(){
			alert('wtf)j')
			d3.select("g").remove()
		}