

var salary_info;
var data_file = "./data/salaries.csv";
var props;
var hist_svg;
var init_bins = 20;
var hist_bins = init_bins;
var current_fields = [ 'Base' ];
var current_position = 'All';
// We want small numbers to still be somewhat visible
var min_height = 8;
var first_time = true;
var minimum_wage = 20*2000;
var wage_filter = 1;
var salary_mult = 1;
var xScale;

    // Load Data then call other functions
    // Avoid havign all data in the callback.
    var load_data = function () {
      d3.csv(data_file, function ( data ) {
       salary_info = data;
       create_histogram([current_fields], true, hist_bins);
     });
    };

    var create_histogram = function ( fields, redraw, bins ) {
      //Filter should be an array of the fields we want to consider for the the chart
      //for example EE, Base, Ot etc. so an example filter is ["OT", "Base"] 
      //in which case we will filter out only these two for considerations

      //Sizing and spacing parameters
      var w = 800;
      var h = 500;
      var x_pad = 60;
      var y_pad = 50;
      var spacing = 20;
      var y_shift = 50;
      h = h + y_shift;

      // If called without a filter default total compensation

      //create a historgram
      histogram = d3.layout.histogram()
      .value( function ( data ) {
        var sum = 0;
          //Basically the 
          for (var i = 0; i < fields.length; i++) {
            // 10 is the radix since we are parsing integers
            sum += salary_mult * parseInt( data[String(fields[i])], 10 );
          }
          return sum;
        })
      .bins(bins);


      if ( current_position != 'All' ) {
        dataset = _.filter(salary_info, function ( data ) {
          var valid = (current_position == data.Title);
          return valid;
        });
      } else {
        dataset = salary_info;
      }

      dataset = _.filter(dataset, function ( data ) {
        return parseInt(data.Base, 10) > wage_filter;
      });

      var data_values = _.map( dataset, function ( data ) {
        var sum = 0;
        for (var i = 0; i < current_fields.length; i++) {
              // 10 is the radix since we are parsing integers
              sum += salary_mult * parseInt( data[String(current_fields[i])], 10 );
            }
            return sum;      });

      var data_mean = _.reduce(data_values, function ( sum, data ) {
        return sum + data;
      }) / data_values.length;

      data_values = _.sortBy(data_values, function(num) { return num; });

      var data_median = function () {
        if ( data_values.length % 2 !== 0 ) {
          return ( data_values[ Math.floor(data_values.length/2)]);
        } else {
          return ( data_values[ Math.floor(data_values.length/2)]);
          //return ( data_values[data_values.length/2] + data_values[data_values.length/2 -1 ])/2;
        }
      }();

      // Get the count of each salary rounded to then nearest 1000s
      var salary_count = _.countBy(data_values, function(d){
        return Math.round( d / 1000) * 1000;
      });
      // Find the # of occurance of the mode
      var data_max_count = _.max(salary_count);
      var data_mode = _.findKey(salary_count, function ( d ){
        return d == data_max_count;
      });

      dataset = histogram(dataset);

      // Create  scales
      xScale = d3.scale.linear()
      .domain([d3.min(dataset, function ( data ) {
        return data.x;
      }), d3.max(dataset, function ( data ) {
        return data.x+data.dx;
      })])
      .range([ x_pad, 800]);

      var yScale = d3.scale.linear()
        // set the domain from 0 to the largest element in group
        .domain( [0, d3.max(dataset, function ( data ) {
          return data.y;
        })])
        .range( [ min_height, h - y_pad - y_shift] );


      //Create reverse scale for axis
      var yScale2 = d3.scale.linear()
        // set the domain from 0 to the largest element in group
        .domain( [0, d3.max(dataset, function ( data ) {
          return data.y;
        })])
        .range( [ h - y_pad - y_shift , min_height ] );

      // Define a function to format the axis
      var round_to = function ( base ) {
        return function ( num ) {
          num = Math.floor( num / base );
          // the d3.format returns a function 
          // initialign with s returns a function that appends
          // the SI unit to the number
          return d3.format( "s" ) ( num );
        };
      };
      // Define the axises
      var xAxis = d3.svg.axis()
      .scale(xScale)
      .orient("bottom")
      .tickFormat(round_to(1000))
      .ticks(bins);

      var yAxis = d3.svg.axis()
      .scale(yScale2)
      .orient("left");

      //Get the object we are lookign for
      if ( redraw ) {
        d3.select("#histogram").select('svg').remove();
        hist_svg = d3.select("#histogram").append("svg")
        .attr("width", w)
        .attr("height", h);
      }


      //Append/update the rects to the SVG
      var bars = hist_svg.selectAll("rect")
      .data(dataset);

      if ( redraw ) {
        bars = bars.enter()
        .append("rect");

        bars.on("mouseover", function ( d ) {

          // 
          if ( d.y === 0 ){
            return;
          }
          d3.select(this)
          .attr("fill", "cornflowerblue");

          var hist_pos  = $("#histogram").position();

          var xPosition = hist_pos.left + parseFloat(d3.select(this).attr("x")) + ((w-x_pad)/bins) / 2;
          var yPosition = hist_pos.top + parseFloat(d3.select(this).attr("y")) / 2 + h / 2.5;

          var values = _.map( d, function ( data ) {
            var sum = 0;
            for (var i = 0; i < current_fields.length; i++) {
              // 10 is the radix since we are parsing integers
              sum += salary_mult * parseInt( data[String(current_fields[i])], 10 );
            }
            return sum;
          });

          var min_value = d3.min(values);
          var max_value = d3.max(values);
          var mean = _.reduce( values, function ( sum, value ) {
            return sum + value;
          }) / values.length;

          var median = function ( values ) {
            if ( (values.length  % 2 ) === 0 ) {
              return ( values[  values.length / 2 ] + values [ values.length /2 - 1 ] ) / 2;
            } else {
              return values [ Math.floor( values.length / 2 )];
            }
          } ( values );


          var format = d3.format('$,.0f');
          var info = "Count: " + d.y + '<br/>';
          info += 'Range: ' + format(min_value) + '-' + format(max_value) + '<br />';
          info += 'Mean: ' + format(mean) + '<br />';
          info += 'Median: ' + format(median) + '<br />';
          //Update the tooltip position and value
          d3.select("#tooltip")
          .style("left", xPosition + "px")
          .style("top", yPosition + "px")
          .select("#value")
          .html(info);

          //Show the tooltip
          d3.select("#tooltip").classed("hidden", false);
        })
.on("mouseout", function(d) {
  // If there are no values we don't want a tooltip
  // or color change
  if ( d.y === 0 ) return;

  d3.select("#tooltip").classed("hidden", true);
  d3.select(this)
  .transition()
  .duration(250)
  .attr("fill", "black");
});

}

bars.attr("class", "rect")
.transition()
.ease('cubic')
.duration( function ( d) {
  return ( d.y > 0 ) ? 1000 : 0;
})
.attr({
  x: function ( d, i ) {
    return xScale( d.x );
  },
  y: function ( d, i ) {
    return h - y_pad - yScale( d.y );
  },
  width: function (d) {
    var binw = xScale( d.x  + d.dx) - xScale(d.x) - spacing/bins;
    if ( binw < 0 ) {
      binw = w - x_pad;
    }
    return binw;
  },
  height: function ( d ) {
    return yScale( d.y );
  },
  fill: function ( d ) {
    //If this has no height, make it invisible
    return ( d.y > 0 ) ? 'black' : 'white';
  }
});

hist_svg.selectAll(".stats").remove();

var stats_x = x_pad;
var info_delta = 150;
var stats_x_delta = 130;

hist_svg.append("text")
.text("Statistical Information: ")
.attr("class", "stats")
.attr("transform", "translate(" +  stats_x  + ", " + y_shift*0.5 + ")" );

hist_svg.append("text")
.text("Mean - $" + d3.format(',.0f')(data_mean))
.attr("class", "stats")
.attr("transform", "translate(" +  stats_x  + ", " + y_shift*0.75 + ")" );

stats_x += stats_x_delta;
hist_svg.append("text")
.text("Median - $" + d3.format(',.0f')(data_median))
.attr("class", "stats")
.attr("transform", "translate(" +  stats_x  + ", " + y_shift*0.75 + ")" );

stats_x += stats_x_delta;
hist_svg.append("text")
.text("Mode - $" + d3.format(',.0f')(data_mode))
.attr("class", "stats")
.attr("transform", "translate(" +  stats_x  + ", " + y_shift*0.75 + ")" );

if ( redraw ) {
        //Append the axis to the SVG
        hist_svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0, " + ( h - y_pad ) + ")" )
        .call(xAxis)
        .selectAll("text")
        .attr("dx", "-20px")
        .attr("dy", "3px")
        .attr("transform", "rotate(-65)");

           //Append the y axis
           hist_svg.append("g")
           .attr("class", "y axis")
           .attr("transform", "translate(" + x_pad +"," + y_shift + ")")
           .call(yAxis);

           var hist_pos  = $("#histogram").position();

           hist_svg.append("text")
           .text("# of Employees")
           .attr("transform", "rotate(-90)" )
           .attr("transform", "translate(" + x_pad / 4  + ", " + ( h - y_pad ) / 2 + ") rotate(-90)" );

           hist_svg.append("text")
           .text("Amount in Thousands")
           .attr("transform", "translate(" + ( w - x_pad ) / 2  + ", " + ( h - y_pad/10 ) + ")" );
         } else {

          hist_svg.select(".x.axis")
          .transition()
          .duration(1000)
          .call(xAxis)
          .selectAll("text")
          .attr("dx", "-20px")
          .attr("dy", "3px")
          .attr("transform", "rotate(-65)");

          hist_svg.select(".y.axis")
          .transition()
          .duration(1000)
          .call(yAxis);
        }

      // Otherwise we need to adjust the axis

      //Get a list of the properties
      props = [];

      for (var p in dataset[0][0]) {
        if (p != "Entity" && p != "Title" && p != "Subtitle" && p != "Total")
          props.push(p);
      }


     //Create the labels and check boxes
     if ( first_time ) {

      d3.select("#histogram_controls")
      .append('h4')
      .text("Display Options");

        //Get A Unique Lists of Titles
        var titles = _.unique(_.pluck(salary_info, 'Title')).sort();
        titles = _.union(['All'], titles);

        d3.select("#histogram_controls")
        .append('select')
        .attr('onChange', 'filter_by_title(this)')
        .selectAll("option")
        .data( titles )
        .enter()
        .append("option")
        .text( function ( d ) {
          return d;
        } )
        .attr( 'value', function ( d ) {
          return d;
        });


      // update the controls with check boxes
      d3.select("#histogram_controls")
      .append('div')
      .selectAll("label")
      .data(props)
      .enter()
      .append('label')
      .attr("class", "checkbox inline")
      .text(function(d) { return d; })
      .append("input")
      .attr("type", "checkbox")
      .attr("name","payout_types")
      .attr("id", function (d) { return d; })
      .attr("value", function (d){return d;})
      .attr("onClick", "updateChartFields()");

      var bart_filters = d3.select("#histogram_controls")
        .append('div');

      bart_filters.append('label')
        .attr('class', 'checkbox inline')
        .text('Minimum Wage Filter')
        .append("input")
        .attr("type", "checkbox")
        .attr("name", "minimum_wage")
        .attr("onClick", "switch_wage_filter()" );


      bart_filters.append('label')
        .attr('class', 'radio')
        .text('Status Quo')
        .append("input")
        .attr("type", "radio")
        .attr("name", "future_pay")
        .attr("id", "status_quo")
        .attr("onClick", "set_multiplier(1)" );

      bart_filters.append('label')
        .attr('class', 'radio')
        .text('What BART Unions are Demanding')
        .append("input")
        .attr("type", "radio")
        .attr("name", "future_pay")
        .attr("onClick", "set_multiplier(1.23)" );

    bart_filters.append('label')
        .attr('class', 'radio')
        .text('What Management Is Offering')
        .append("input")
        .attr("type", "radio")
        .attr("name", "future_pay")
        .attr("onClick", "set_multiplier(1.08)" );


      first_time = false;
      $('#'+current_fields[0]).click();
      $('#status_quo').click();



    }


  };

  var set_multiplier = function ( value ) {
    salary_mult = value;
    create_histogram(current_fields, false, hist_bins);
  };

  var filter_by_title = function ( sel ) {
    current_position = sel.options[sel.selectedIndex].value;
    update_redraw();
  };

  var switch_wage_filter = function () {
    wage_filter = ( wage_filter == minimum_wage ) ? 1 : minimum_wage;
    create_histogram(current_fields, false, hist_bins);
  };

    // Increase Bin Count
    var increase_bins = function () {
      hist_bins++;
      update_redraw();
    };

    // Decrease Bin Count
    var decrease_bins = function () {
      hist_bins--;
      update_redraw();
    };

    var reset = function (){
      hist_bins = init_bins;
      update_redraw();
    };

    // Alter the chart fields
    // Does not require a redraw
    var updateChartFields = function () {

        //Get the types of payouts
        var cboxes = document.getElementsByName('payout_types');
        var len = cboxes.length;
        var new_fields = [];

        //Push the new values into the array
        for (var i=0; i<len; i++) {
          if ( cboxes[i].checked ) {
            new_fields.push(cboxes[i].value);
          }
        }

       //Update the global current fields
       current_fields = new_fields;
       create_histogram(current_fields, false, hist_bins);

     };

     var update_redraw = function () {
       create_histogram(current_fields, true, hist_bins);
     };

     load_data();

     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-42755764-1', 'timiokah.com');
     ga('send', 'pageview');

